<?php

/**
 * @file
 * Provide views runtime hooks for my_module.module.
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_substitutions().
 */
function view_draft_permission_views_query_substitutions(ViewExecutable $view) {
  $account = \Drupal::currentUser();

  // Show unpublished entities if user has at least one permission.
  $view_unpublished = $account->hasPermission('view entities which state is draft') ||
                      $account->hasPermission('view any unpublished content');
  return [
    '***VIEW_ANY_UNPUBLISHED_NODES***' => intval($view_unpublished),
  ];
}
